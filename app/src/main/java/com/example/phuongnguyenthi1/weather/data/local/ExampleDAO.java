package com.example.phuongnguyenthi1.weather.data.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;


import com.example.phuongnguyenthi1.weather.model1.Example;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface ExampleDAO {
    @Query("SELECT * FROM examples")
    List<Example> getAllExample();
    @Update
    void updateExample(Example ... examples);
    @Insert
    void insertExample(Example ... examples);
}
