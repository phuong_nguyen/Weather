package com.example.phuongnguyenthi1.weather.model1;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.TypeConverters;

import com.example.phuongnguyenthi1.weather.util.DataConverter;

import java.util.ArrayList;
import java.util.List;

public class Hourly {

    @ColumnInfo(name = "icon_hourly")
    private String icon;
    @ColumnInfo(name = "data_hourly")
    @TypeConverters(DataConverter.class)
    private List<DataHourly> data = new ArrayList<>();

    public Hourly() {
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<DataHourly> getData() {
        return data;
    }

    public void setData(List<DataHourly> data) {
        this.data = data;
    }
}
