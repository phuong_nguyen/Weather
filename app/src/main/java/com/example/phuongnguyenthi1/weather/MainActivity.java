package com.example.phuongnguyenthi1.weather;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.phuongnguyenthi1.weather.api.ApiUtils;
import com.example.phuongnguyenthi1.weather.api.CurrentlyAPI;
import com.example.phuongnguyenthi1.weather.data.ExampleRepository;
import com.example.phuongnguyenthi1.weather.data.local.ExampleDatabase;
import com.example.phuongnguyenthi1.weather.data.local.ExampleLocalDataSource;
import com.example.phuongnguyenthi1.weather.model1.Currently;
import com.example.phuongnguyenthi1.weather.model1.Daily;
import com.example.phuongnguyenthi1.weather.model1.DataDaily;
import com.example.phuongnguyenthi1.weather.model1.DataHourly;
import com.example.phuongnguyenthi1.weather.model1.Example;
import com.example.phuongnguyenthi1.weather.model1.Hourly;
import com.example.phuongnguyenthi1.weather.util.Network;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.jjoe64.graphview.GraphView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity  {

    public static final String TAG = "TAG";
    private final static int ALL_PERMISSION_RESULT = 101;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationManager mLocationManager;
    private Location mLocation;
    private boolean mIsGPSEnable = false;
    private boolean mIsNetWorkEnable = false;


    //database
 //   private ExampleRepository mExampleRepository;
    private List<Example> mListExample = new ArrayList<>();
    //currently
    private TextView mTvCurrentTemperature;
    private TextView mTvCurrentPlace;
    private TextView mTvCurrentSummary;
    private TextView mTVCurrentTime;
    private TextView mTvCurrentHumidity;
    private TextView mTvCurrentUvIndex;
    // hourly
    private LineChart mHourlyLcTemperature;
    private LinearLayout mLlHourly ;

    //daily
    private LineChart mDailyLcTemperature;
    private LinearLayout mLlDaily;

    CurrentlyAPI mCurrentlyAPI;

    GraphView mGvHourlyLineChart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //database
      //  mExampleRepository = ExampleRepository.getInstance(ExampleLocalDataSource.getInstace(exampleDatabase.exampleDAO()));
        // currently
        mTvCurrentPlace = findViewById(R.id.tv_current_place);
        mTvCurrentTemperature = findViewById(R.id.tv_current_temperature);
        mTvCurrentSummary = findViewById(R.id.tv_current_summary);
        mTVCurrentTime = findViewById(R.id.tv_current_time);
        mTvCurrentHumidity = findViewById(R.id.tv_current_humidity);
        mTvCurrentUvIndex = findViewById(R.id.tv_current_uv_index);

        // hourly
        mHourlyLcTemperature = findViewById(R.id.hourly_line_chart_temperature);
        mHourlyLcTemperature.getAxisLeft().setDrawLabels(false);
        mHourlyLcTemperature.getAxisRight().setDrawLabels(false);
        mHourlyLcTemperature.getXAxis().setDrawLabels(false);
        mHourlyLcTemperature.getLegend().setEnabled(false);
        mLlHourly = findViewById(R.id.ll_hourly);
        mLlHourly.setOrientation(LinearLayout.HORIZONTAL);

        // daily
        mDailyLcTemperature = findViewById(R.id.daily_line_chart_temperature);
        mDailyLcTemperature.getAxisLeft().setDrawLabels(false);
        mDailyLcTemperature.getAxisRight().setDrawLabels(false);
        mDailyLcTemperature.getXAxis().setDrawLabels(false);
        mDailyLcTemperature.getLegend().setEnabled(false);
        mLlDaily= findViewById(R.id.ll_daily);
        mLlDaily.setOrientation(LinearLayout.HORIZONTAL);

        PlaceAutocompleteFragment autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                // TODO: Get info about the selected place.
                Log.i("TAG", "Place: " + place.getName());
                mTvCurrentPlace.setText(place.getName());
                LatLng latLng = place.getLatLng();
                getAPI(latLng.latitude, latLng.longitude);
            }

            @Override
            public void onError(Status status) {
                // TODO: Handle the error.
                Log.i("TAG", "An error occurred: " + status);
            }
        });
        getData();
        getLocation();
    }

    private void  getData() {

        mListExample =  ExampleDatabase.getInstance(this).exampleDAO().getAllExample();
        if( mListExample != null &&  mListExample.size() != 0){
            Example example = mListExample.get(0);
            Log.d("latitude", example.getCurrently().getSummary()+"");
            updateUI(example);
        }else {
            Toast.makeText(this, "list example null", Toast.LENGTH_SHORT).show();
        }
    }



    private void onGetAllExampleFailure(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void onGetAllExampleSuccess(ArrayList<Example> examples){

    }

    private void getLocation() {

        mLocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        mIsGPSEnable = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        mIsNetWorkEnable = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            // ask permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }else {
            //have permission
            if(!mIsGPSEnable && ! mIsNetWorkEnable){
                showSettingAlert();
            }
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // got last known location. In some rare situations this can be null
                            if (location != null) {
                                Log.d(TAG, "can get location");
                                mLocation = location;
                                // do some things
                                getAPI(location.getLatitude(), location.getLongitude());

                            }else {
                                Log.d(TAG, "can not  get location");
                            }
                        }
                    });
        }

    }

    private void showSettingAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("GPS is not Enabled");
        alertDialog.setMessage("Do you want to turn on GPS?");
        alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, 10);
                getLocation();
            }
        });
        alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        alertDialog.show();
    }

    public void getAPI(final double latitude, final double longitude) {

        if(Network.isNetworkConnected(this)){
            mCurrentlyAPI = ApiUtils.getCurrentlyAPI();
            String locate = latitude + "," + longitude;
            mCurrentlyAPI.getAnswers(locate).enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    String namePlace = getPlaceFromLocation(latitude, longitude);
                    mTvCurrentPlace.setText(namePlace);
                    try {
                        Example example = getExample(response.body().string());
                        Log.d("latitude", example.getLongitude()+ " APi");
                        ExampleDatabase.getInstance(getApplicationContext()).exampleDAO().updateExample(example);
                        updateUI(example);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                }
            });
        }else {

            Toast.makeText(this, getResources().getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }



    }

    private String getPlaceFromLocation(double latitude, double longitude) {
        String name = "";
        Geocoder geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
        try {
            List<Address> listAddresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (null != listAddresses && listAddresses.size() > 0) {
                name = listAddresses.get(0).getCountryName();
            } else {
            }
        } catch (IOException e) {
            e.printStackTrace();
            Log.d(TAG, "error:  " + e.getMessage());
        }
        return name;
    }

    private void updateUI(Example example) {

        //update currently
        Currently currently = example.getCurrently();
       mTvCurrentSummary.setText(currently.getSummary());
        mTVCurrentTime.setText(convertLongToDateTime(currently.getTime()));
        mTvCurrentTemperature.setText(convertFToC(currently.getTemperature()) + " \u2103");
        mTvCurrentHumidity.setText(getHumidity(currently.getHumidity()) + " %");
        if( currently.getUvIndex() < 5){
            mTvCurrentUvIndex.setText("Thấp");
        }else {
            mTvCurrentUvIndex.setText("Cao");
        }

        // update hourly
        Hourly hourly = example.getHourly();
        ArrayList<Entry> entries = new ArrayList<>();
        for( int i=0; i< 5; i ++){
            DataHourly dataHourly = hourly.getData().get(i*4);
            entries.add(new Entry(i, convertFToC(dataHourly.getTemperature())));
        }
//
        mLlHourly.removeAllViews();
        for( int i=0; i< 5; i++){
            DataHourly dataHourly = hourly.getData().get(i*4);

//            entries.add(new Entry((float)convertFToC(dataHourly.getTemperature()), if));
            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.item_view, null);
            TextView mTvHourlyHumidity = view.findViewById(R.id.item_hourly_tv_humidity);
            TextView mTvHourlyTime = view.findViewById(R.id.item_hourly_tv_time);

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    1
            );
            view.setLayoutParams(param);

            mTvHourlyHumidity.setText(getHumidity(dataHourly.getHumidity()) + " %");
            mTvHourlyTime.setText(convertLongToTime(dataHourly.getTime()));
            mLlHourly.addView(view);
        }
        LineDataSet dataSet = new LineDataSet(entries, "temperature");
        dataSet.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet.setCircleColor(Color.WHITE);
        dataSet.setColor(Color.WHITE);
        dataSet.setLineWidth(1f);
        dataSet.setCircleRadius(3f);
        dataSet.setValueTextSize(9f);
        dataSet.setHighLightColor(Color.WHITE);
        List<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(dataSet);
        LineData lineData = new LineData( dataSets);
        mHourlyLcTemperature.setDescription(null);
        mHourlyLcTemperature.setData(lineData);
        mHourlyLcTemperature.invalidate();

        //update daily
        Daily daily = example.getDaily();
        ArrayList<Entry> entriesDailyHigh = new ArrayList<>();
        ArrayList<Entry> entriesDailyLow = new ArrayList<>();
        mLlDaily.removeAllViews();
        for( int i=0; i< 5; i++){
            DataDaily dataDaily = daily.getData().get(i);
            entriesDailyHigh.add(new Entry(i, convertFToC(dataDaily.getTemperatureHigh())));
            entriesDailyLow.add(new Entry(i, convertFToC(dataDaily.getTemperatureLow())));
            LayoutInflater layoutInflater = (LayoutInflater) getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.item_view, null);
            TextView mTvHourlyHumidity = view.findViewById(R.id.item_hourly_tv_humidity);
            TextView mTvHourlyTime = view.findViewById(R.id.item_hourly_tv_time);

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    1
            );
            view.setLayoutParams(param);
            mTvHourlyHumidity.setText(getHumidity(dataDaily.getHumidity()) + " %");
            mTvHourlyTime.setText(convertLongToDay(dataDaily.getTime()));
            mLlDaily.addView(view);
        }

        LineDataSet setHigh = new LineDataSet(entriesDailyHigh, "high");
        setHigh.setAxisDependency(YAxis.AxisDependency.LEFT);
        LineDataSet setLow = new LineDataSet(entriesDailyLow, "low");
        setLow.setAxisDependency(YAxis.AxisDependency.LEFT);

        List<ILineDataSet> dataSetsDaily = new ArrayList<>();
        dataSetsDaily.add(setHigh);
        dataSetsDaily.add(setLow);
        LineData dataDaily = new LineData(dataSetsDaily);
        mDailyLcTemperature.setData(dataDaily);
        mDailyLcTemperature.setDescription(null);
        mDailyLcTemperature.invalidate();
    }

    private int getHumidity(Double humidity){
        int temp = (int) (humidity * 100);
        return temp;
    }
    private String convertLongToDay(long time) {
        SimpleDateFormat format = new SimpleDateFormat("E");
        String string = format.format(new Date(time*1000L));
        return string;
    }

    public void setIconTemperature(String iconTemperature) {
        switch (iconTemperature){
        }
    }

    private int convertFToC(Double temperature) {
        int result = (int)(temperature - 32)*5/9; // + " \u2103";
        return result;
    }

    private String convertLongToTime(long time) {
        SimpleDateFormat format = new SimpleDateFormat("H:ss");
        String string = format.format(new Date(time*1000L));
        return string;
    }
    private String convertLongToDateTime(long time){
        SimpleDateFormat format = new SimpleDateFormat("E, d-MM HH:ss");
        String dateTime = format.format(new Date(time*1000L));
        return dateTime;
    }

    private Example getExample(String string) {
        Example example = new Example();

        try {
            JSONObject root = new JSONObject(string);

            // get currently
            JSONObject currentlyJS = root.optJSONObject("currently");
            Currently currently = getCurrently(currentlyJS);

            // get hourly
            JSONObject hourlyJS = root.getJSONObject("hourly");
            Hourly hourly = getHourly(hourlyJS);

            // get daily
            JSONObject dailyJs = root.getJSONObject("daily");
            Daily daily = getDaily(dailyJs);

            example.setCurrently(currently);
            example.setHourly(hourly);
            example.setDaily(daily);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return example;
    }

    private Daily getDaily(JSONObject dailyJs) {
        Daily daily = new Daily();
        try {
            JSONArray dailyArray = dailyJs.getJSONArray("data");
            ArrayList<DataDaily> listDataDaily = new ArrayList<>();
            listDataDaily = getListDataDaily(dailyArray);
            daily.setData(listDataDaily);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return daily;
    }

    private ArrayList<DataDaily> getListDataDaily(JSONArray dailyArray) {
        ArrayList<DataDaily> listDataDaily = new ArrayList<>();
        for( int i = 0; i< dailyArray.length(); i++){
            try {
                JSONObject temp = dailyArray.getJSONObject(i);
                DataDaily dataDaily = getDataDaily(temp);
                listDataDaily.add(dataDaily);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return listDataDaily;
    }

    private DataDaily getDataDaily(JSONObject temp) {
        DataDaily dataDaily = new DataDaily();
        try {
            long time = temp.getLong("time");
            String icon = temp.getString("icon");
            String summary = temp.getString("summary");
            double temperatureHigh = temp.getDouble("temperatureHigh");
            double temperatureLow = temp.getDouble("temperatureLow");
            double humidity = temp.getDouble("humidity");
            dataDaily = new DataDaily(time, icon , summary, temperatureHigh, temperatureLow, humidity);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dataDaily;
    }

    private Hourly getHourly(JSONObject hourlyJS) {
        Hourly hourly = new Hourly();
        try {
            JSONArray dataArray = hourlyJS.getJSONArray("data");
            ArrayList<DataHourly> listDataHourly = new ArrayList<>();
            listDataHourly = getListDatum_(dataArray);
            hourly.setData(listDataHourly);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return hourly;
    }

    private ArrayList<DataHourly> getListDatum_(JSONArray dataArray) {
        ArrayList<DataHourly> listDataHourly = new ArrayList<>();
        for( int i =0; i< dataArray.length(); i++){
            try {
                JSONObject temp = dataArray.getJSONObject(i);
                DataHourly  dataHourly = getDataHourly(temp);
                listDataHourly.add(dataHourly);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return listDataHourly;
    }

    private DataHourly getDataHourly(JSONObject temp) {
        DataHourly dataHourly = new DataHourly();
        try {
            long time = temp.getLong("time");
            String summary = temp.getString("summary");
            String icon = temp.getString("icon");
            double temperature = temp.getDouble("temperature");
            double humidity = temp.getDouble("humidity");
            dataHourly  = new DataHourly(time, summary, icon, temperature, humidity);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dataHourly;
    }

    private Currently getCurrently(JSONObject currentlyJS) {
        long timeCurrently = 0;
        Currently currently = new Currently();
        try {
            timeCurrently = currentlyJS.getLong("time");
            String summary = currentlyJS.getString("summary");
            double temperature = currentlyJS.getDouble("temperature");
            String icon = currentlyJS.getString("icon");
            Double humidity = currentlyJS.getDouble("humidity");
            int uvIndex = currentlyJS.getInt("uvIndex");
            currently = new Currently(timeCurrently, summary, temperature, icon, humidity,uvIndex);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return currently;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if( requestCode == 1){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                if( !mIsGPSEnable  && ! mIsNetWorkEnable){
                    showSettingAlert();
                }
                mFusedLocationClient.getLastLocation()
                        .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                // got last known location. In some rare situations this can be null
                                if (location != null) {
                                    mLocation = location;
                                    // do some things

                                }
                            }
                        });

            }
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if( requestCode == 10){
            getLocation();
        }
    }


}
