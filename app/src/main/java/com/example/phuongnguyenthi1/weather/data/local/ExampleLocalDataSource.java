package com.example.phuongnguyenthi1.weather.data.local;

import com.example.phuongnguyenthi1.weather.data.ExampleDataSource;
import com.example.phuongnguyenthi1.weather.model1.Example;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

public class ExampleLocalDataSource implements ExampleDataSource {

    private static ExampleLocalDataSource sInstance;

    public static ExampleLocalDataSource getInstace(ExampleDAO exampleDAO){
        if( sInstance == null){
            sInstance = new ExampleLocalDataSource(exampleDAO);
        }
        return sInstance;
    }

    private ExampleDAO mExampleDAO;

    public ExampleLocalDataSource( ExampleDAO exampleDAO){
        this.mExampleDAO = exampleDAO;
    }

    @Override
    public List<Example> getAllExample() {
        return null;
    }

    @Override
    public void updateExample(Example... examples) {

    }

    @Override
    public void insertExample(Example... examples) {


    }
}
