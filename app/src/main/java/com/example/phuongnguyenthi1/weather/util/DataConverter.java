package com.example.phuongnguyenthi1.weather.util;

import android.arch.persistence.room.TypeConverter;

import com.example.phuongnguyenthi1.weather.model1.DataDaily;
import com.example.phuongnguyenthi1.weather.model1.DataHourly;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class DataConverter {
    private static Gson gson = new Gson();

    //convert datahourly
    @TypeConverter
    public static List<DataHourly> stringToDataHourlyList(String data){
        if( data == null){
            return  Collections.emptyList();
        }
        Type listType = new TypeToken<List<DataHourly>>(){}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static  String dataHourlyListToString(List<DataHourly> someObjects){
        return gson.toJson(someObjects);
    }



}
