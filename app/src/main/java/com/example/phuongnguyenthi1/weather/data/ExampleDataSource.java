package com.example.phuongnguyenthi1.weather.data;


import com.example.phuongnguyenthi1.weather.model1.Example;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

public interface ExampleDataSource {

    List<Example> getAllExample();
    void updateExample(Example ... examples);
    void insertExample(Example ... examples);
}
