package com.example.phuongnguyenthi1.weather.model1;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.TypeConverters;

import com.example.phuongnguyenthi1.weather.util.DataDailyConverter;

import java.util.ArrayList;
import java.util.List;

public class Daily {

    @ColumnInfo(name = "icon_daily")
    private String icon;
    @ColumnInfo(name = "data_daily")
    @TypeConverters(DataDailyConverter.class)
    private List<DataDaily> data = new ArrayList<>();

    public Daily() {
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public List<DataDaily> getData() {
        return data;
    }

    public void setData(List<DataDaily> data) {
        this.data = data;
    }
}
