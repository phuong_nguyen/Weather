package com.example.phuongnguyenthi1.weather.model1;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.TypeConverter;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.example.phuongnguyenthi1.weather.util.DataConverter;
import com.example.phuongnguyenthi1.weather.util.DataDailyConverter;


@Entity(tableName = "examples", primaryKeys = {"latitude", "longitude"})
public class Example {
    @NonNull
    private double latitude;
    @NonNull
    private double longitude;
    @Embedded
    private Currently currently;
    @Embedded

    private Hourly hourly;
    @Embedded

    private Daily daily;

    public Example() {
    }

    @NonNull
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(@NonNull double latitude) {
        this.latitude = latitude;
    }

    @NonNull
    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(@NonNull double longitude) {
        this.longitude = longitude;
    }

    public Currently getCurrently() {
        return currently;
    }

    public void setCurrently(Currently currently) {
        this.currently = currently;
    }

    public Hourly getHourly() {
        return hourly;
    }

    public void setHourly(Hourly hourly) {
        this.hourly = hourly;
    }

    public Daily getDaily() {
        return daily;
    }

    public void setDaily(Daily daily) {
        this.daily = daily;
    }
}
