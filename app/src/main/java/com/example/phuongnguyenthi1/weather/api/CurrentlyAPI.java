package com.example.phuongnguyenthi1.weather.api;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface CurrentlyAPI {

//    "https://api.darksky.net/forecast/9e061d58d07521d4c5df7391ea35302a/37.8267,-122.4233"
    @GET("/forecast/9e061d58d07521d4c5df7391ea35302a/37.8267,-122.4233")
    Call<ResponseBody> getBody();

    @GET("/forecast/9e061d58d07521d4c5df7391ea35302a/{location}")
    Call<ResponseBody> getAnswers(@Path("location") String tags);
}
