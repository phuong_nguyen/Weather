package com.example.phuongnguyenthi1.weather.util;

import android.arch.persistence.room.TypeConverter;

import com.example.phuongnguyenthi1.weather.model1.DataDaily;
import com.example.phuongnguyenthi1.weather.model1.DataHourly;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class DataDailyConverter {

    private static Gson gson = new Gson();
    //convery data daily
    @TypeConverter
    public static List<DataDaily> stringToDataDailyList(String data){
        if( data == null){
            return  Collections.emptyList();
        }
        Type listType = new TypeToken<List<DataDaily>>(){}.getType();

        return gson.fromJson(data, listType);
    }

    @TypeConverter
    public static  String dataDailyListToString(List<DataDaily> someObjects){
        return gson.toJson(someObjects);
    }
}
