package com.example.phuongnguyenthi1.weather.data;


import android.util.Log;

import com.example.phuongnguyenthi1.weather.model1.Example;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

public class ExampleRepository implements ExampleDataSource {

    private static ExampleRepository sInstance;

    public static ExampleRepository getInstance( ExampleDataSource localDataSource){
        if( sInstance == null){
            sInstance = new ExampleRepository(localDataSource);
        }
        return sInstance;
    }

    private ExampleDataSource mLocalDataSource;

    public ExampleRepository(ExampleDataSource localDataSource){
        this.mLocalDataSource = localDataSource;
    }
    @Override
    public List<Example> getAllExample() {
        return mLocalDataSource.getAllExample();
    }

    @Override
    public void updateExample(Example... examples) {
        mLocalDataSource.updateExample(examples);
    }

    @Override
    public void insertExample(Example... examples) {
        for(int i=0;i<examples.length;i++)
        {
            Log.d("insert", "insert example");
            mLocalDataSource.insertExample(examples[i]);
        }

    }
}
