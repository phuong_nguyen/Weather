package com.example.phuongnguyenthi1.weather.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;


import com.example.phuongnguyenthi1.weather.model1.Example;

import static com.example.phuongnguyenthi1.weather.data.local.ExampleDatabase.DATABASE_VERSION;


@Database(entities = {Example.class}, version = DATABASE_VERSION)
public abstract class ExampleDatabase extends RoomDatabase {

    private static ExampleDatabase sExampleDatabase;
    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "Room_database";

    public abstract ExampleDAO exampleDAO();

    public static ExampleDatabase getInstance(Context context){
        if( sExampleDatabase == null){
            sExampleDatabase = Room.databaseBuilder(context, ExampleDatabase.class, DATABASE_NAME)
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return sExampleDatabase;
    }

}
